import express from 'express';
const router = express.Router();
import { setupDB } from './db';

const findProject = async (req, res, next) => {
  const db = await setupDB();
  const { slug } = req.params;

  const project = await db.collection('projects').findOne({ slug });

  if (!project) {
    res.status(400).json({ error: `project ${slug} does not exist` });
  } else {
    req.body.project = project;
    next();
  }
};

const findBoard = async (req, res, next) => {
  const { project } = req.body;
  const { name } = req.params;
  const boardID = project.boards.findIndex(board => board.name === name);

  if (boardID === -1) {
    res.status(400).json({ error: `board ${name} does not exist` });
  } else {
    req.body.boardID = boardID;
    next();
  }
};

const findTask = async (req, res, next) => {
  const { project, boardID } = req.body;
  const { id } = req.params;
  const taskID = project.boards[boardID].tasks.findIndex(task => task.id === id);

  if (taskID === -1) {
    res.status(400).json({ error: `task ${id} does not exist` });
  } else {
    req.body.taskID = taskID;
    next();
  }
};

// código pt. 2
router.post('/', async (req, res) => {
  const { name } = req.body;
  const db = await setupDB();
  const slug = name.toLowerCase();

  const project = {
    slug,
    boards: [],
  };

  const existingProject = await db.collection('projects').findOne({ slug });

  if (existingProject) {
    res.status(400).json({ error: `project ${slug} already exists` });
  } else {
    await db.collection('projects').insertOne(project);
    res.json({ project });
  }
});

// código pt.3
router.get('/:slug', findProject, async (req, res) => {
  const { project } = req.body;
  res.json({ project });
});

// código pt. 4
router.post('/:slug/boards', findProject, async (req, res) => {
  const db = await setupDB();

  const { name, project } = req.body;
  const board = { name, tasks: [] };
  project.boards.push(board);

  await db
    .collection('projects')
    .findOneAndReplace({ slug: project.slug }, project);

  res.json({ board });
});

////////////////////////////

// código pt. 5
router.delete('/:slug', findProject, async (req, res) => {
  const db = await setupDB();

  const { project } = req.body;

  await db
    .collection('projects')
    .deleteOne({ slug: project.slug });

  res.json({ project });
});

// código pt. 6
router.delete('/:slug/boards/:name', findProject, findBoard, async (req, res) => {
  const db = await setupDB();

  const {project, boardID} = req.body
  const boards = project.boards;

  project.boards.splice(boardID, 1);

  await db
   .collection('projects')
   .findOneAndReplace({ slug: project.slug }, project);

  res.json({ boards });
});

// código pt. 7
router.post('/:slug/boards/:name/tasks', findProject, findBoard, async (req, res) => {
  const db = await setupDB();

  const { project, boardID, id } = req.body;

  project.boards[boardID].tasks.push({ id });

  await db
    .collection('projects')
    .findOneAndReplace({ slug: project.slug }, project);

  res.json({ 'task' : { id } });
});

// código pt. 8
router.delete('/:slug/boards/:name/tasks/:id', findProject, findBoard, findTask, async (req, res) => {
  const db = await setupDB();

  const { project, boardID, taskID } = req.body;
  const { id } = req.params;
  const tasks = project.boards[boardID].tasks;

  project.boards[boardID].tasks.splice(taskID, 1);

  await db
    .collection('projects')
    .findOneAndReplace({ slug: project.slug }, project);

  res.json({ 'task' : { id } });
});

export default router;






















//
