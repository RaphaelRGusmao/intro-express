import request from 'supertest';
import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.3
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 4
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  // código pt. 5
  test('DELETE /projects/:slug deletes a project by slug', async () => {
    const slug = 'awesome'

    await request(app).post('/projects').send({ name: slug });

    const del = await request(app).delete(`/projects/${slug}`);
    expect(del.status).toBe(200);

    const { body } = await request(app).get(`/projects/${slug}`);
    expect(body.error).toBe(`project ${slug} does not exist`);
  });

  // código pt. 6
  test('DELETE /projects/:slug/boards/:name deletes a board from a project', async () => {
    const slug = 'awesome'
    const name = 'todo';

    await request(app).post('/projects').send({ name: slug });
    await request(app).post(`/projects/${slug}/boards`).send({ name });

    const del = await request(app).delete(`/projects/${slug}/boards/${name}`);
    expect(del.status).toBe(200);

    const { body } = await request(app).get(`/projects/${slug}`);
    expect(body.project).toBeDefined();
    expect(body.project.boards.length).toBe(0);
  });

  // código pt. 7
  test('POST /projects/:slug/boards/:name/tasks adds a task into a board from a project', async () => {
    const slug = 'awesome'
    const name = 'todo';
    const id = '123';

    await request(app).post('/projects').send({ name: slug });
    await request(app).post(`/projects/${slug}/boards`).send({ name });

    const { body } = await request(app).post(`/projects/${slug}/boards/${name}/tasks`).send({ id });
    expect(body.task).toBeDefined();
    expect(body.task.id).toBe(id);
  });

  // código pt. 8
  test('DELETE /projects/:slug/boards/:name/tasks/:id deletes a task from a board from a project', async () => {
    const slug = 'awesome'
    const name = 'todo';
    const id = '123';

    await request(app).post('/projects').send({ name: slug });
    await request(app).post(`/projects/${slug}/boards`).send({ name });
    await request(app).post(`/projects/${slug}/boards/${name}/tasks`).send({ id });

    const del = await request(app).delete(`/projects/${slug}/boards/${name}/tasks/${id}`);
    expect(del.status).toBe(200);

    const { body } = await request(app).get(`/projects/${slug}`);
    expect(body.project).toBeDefined();
    expect(body.project.boards).toBeDefined();
    expect(body.project.boards[0].tasks).toBeDefined();
    expect(body.project.boards[0].tasks.length).toBe(0);
  });
});
